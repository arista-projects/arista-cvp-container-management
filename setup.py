import os
import shutil
from setuptools import setup

# Load list of requirements from req file
with open('requirements.txt') as f:
    REQUIRED_PACKAGES = f.read().splitlines()

# Load description from README file
with open("README.rst", "r") as fh:
    LONG_DESCRIPTION = fh.read()

# Rename Script to sync with original name
shutil.copyfile('bin/aristaCvpContainerManager.py', 'bin/arista-cvp-container-manager')

setup(
    name="cvp-configlet-uploader",
    version="0.1",
    scripts=["bin/arista-cvp-container-manager"],
    python_requires=">=2.7",
    install_requires=REQUIRED_PACKAGES,
    url="https://github.com/titom73/arista-cvp-container-management",
    license="BSD",
    author="Thomas Grimonet",
    author_email="tom@inetsix.net",
    description="Tool to manage CVP container remotely using CVP APIs",
    long_description=LONG_DESCRIPTION,
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ]
)
