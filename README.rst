.. image:: https://img.shields.io/badge/License-BSD%203--Clause-blue.svg
  :target: https://github.com/titom73/arista-cvp-container-management/blob/master/LICENSE
  :alt: License Model

.. image:: https://readthedocs.org/projects/arista-cvp-container-management/badge/?version=latest
  :target: https://arista-cvp-container-management.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status

.. image:: https://travis-ci.org/titom73/arista-cvp-container-management.svg?branch=master
    :target: https://travis-ci.org/titom73/arista-cvp-container-management
    :alt: CI Status

Container manager for CoudVision
================================

Generic script to manage containers on `Arista Cloudvision <https://www.arista.com/en/products/eos/eos-cloudvision>`_ server. It is based on `cvprac <https://github.com/aristanetworks/cvprac>`_ library to
interact using APIs calls between your client and CVP interface.

**Supported Features**

-  **Check** if container exists on CVP.
- **Create** a container on CVP topology
- **Delete** a container from CVP topology.
- Get **information** from Cloudvision based on container's name.
- Collect list of attached devices.
- Move a devices to an existing container.

Complete documentation available on `read the doc <https://arista-cvp-container-management.readthedocs.io/en/latest/>`_

Known Issue
-----------

Due to a change in CVP API, change-control needs to get snapshot referenced per
task. Current version of ``cvprack`` does not support it in version 1.0.1

Fix is available in develop version. To install development version, use pip::

   $ pip install git+https://github.com/aristanetworks/cvprac.git@develop


Getting Started
---------------

.. code:: shell


   $ pip install git+https://github.com/titom73/arista-cvp-container-management.git

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='xxx.xxx.xxx.xxx'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   export CVP_TZ='France'
   export CVP_COUNTRY='France'
   EOT

   # run script (assuming VLANs configlet is present on CVP)
   $ arista-cvp-container-management -j actions.json

License
-------

Project is published under `BSD License <https://github.com/titom73/configlet-cvp-uploader/blob/master/LICENSE>`_.

Ask question or report issue
----------------------------

Please open an issue on Github this is the fastest way to get an answer.

Contribute
----------

Contributing pull requests are gladly welcomed for this repository. If
you are planning a big change, please start a discussion first to make
sure we’ll be able to merge it.
