Installation
---------------

Script can be used with 2 different installation method:

- git clone for testing. In this case it is recommended to use a virtual-environment
- Python PIP module to install binary directly to your syste. A virtual-environment is also recommended for testing purpose.

Installation with PIP
~~~~~~~~~~~~~~~~~~~~~

.. code:: shell


   $ pip install git+https://github.com/titom73/arista-cvp-container-management.git

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='xxx.xxx.xxx.xxx'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   EOT

   $ source env.variables.sh

   # run script (assuming VLANs configlet is present on CVP)
   $ arista-cvp-container-management -j actions.json

Git Clone
~~~~~~~~~

It is highly recommended to use Python virtual environment for testing

.. code:: shell

   $ git clone https://github.com/titom73/configlet-cvp-uploader.git

   $ pip install -r requirements.txt

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='xxx.xxx.xxx.xxx'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   EOT

   $ source env.variables.sh

   # run script (assuming VLANs configlet is present on CVP)
   $ bin/aristaCvpContainerManager -j actions.json